import React from 'react'

import {Jumbotron,Button,Row,Col} from 'react-bootstrap'



import {Link} from 'react-router-dom'





export default function Banner({bannerProps}){



	return (

		

			<Row className="app">
				<Col className="container">
					<Jumbotron>
						<h1>{bannerProps.title}</h1>
						<p>{bannerProps.description}</p>
						<Link to={bannerProps.destination} className="btn btn-primary">{bannerProps.label}</Link>
					</Jumbotron>
				</Col>
			</Row>

		

		)

}