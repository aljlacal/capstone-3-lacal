import React from 'react'

//react-bootstrap components
import {Row,Col,Card} from 'react-bootstrap'



export default function Highlights(object){


	return (

		<>
		<h3>WHAT'S HOT</h3>


		<Row>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
					<img src="https://media.istockphoto.com/photos/flower-power-picture-id856279340" />
						<Card.Title>
							<h5>FLOWER POWER NOW</h5>
						</Card.Title>
					</Card.Body>

						<Card.Text>							
							Time to blossom with the far out and groovy footwear pack
						</Card.Text>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
					<img src ="https://brand.assets.adidas.com/image/upload/f_auto,q_auto,fl_lossy/enPH/Images/originals-ss21-lego-energy-launch-footwear-hp-mh-large-d_tcm184-697846.jpg" />
						<Card.Title>
							<h5>TWO ICONS COLLIDE</h5>
						</Card.Title>
					</Card.Body>
						<Card.Text>
							Why? Because we can Feel the summertime pump
						</Card.Text>
				</Card>
			</Col>
			<Col xs={12} md={4}>
				<Card className="cardHighlight">
					<Card.Body>
					<img src ="https://brand.assets.adidas.com/image/upload/f_auto,q_auto,fl_lossy/enPH/Images/specialist_sports-ss21-olympics-tokyo_collection-launch-plp-1-iw_d-d_tcm184-704934.jpg" />
						<Card.Title>
							<h5>TOKYO COLLECTION</h5>
						</Card.Title>
					</Card.Body>
						<Card.Text>
							Multi-sport perform wear, powered by innovation.
						</Card.Text>
				</Card>
			</Col>
		</Row>

</>

		)


}