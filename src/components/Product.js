import React, {useState,useEffect} from 'react'

import {Card,Button,Row,Col,Container} from 'react-bootstrap'



export default function Product({productProp}){

	
	/*Array destructuring*/
	const [count,setCount] = useState(0)
	const [seats,setSeats] = useState(10)
	const [isActive,setIsActive] = useState(true)


	useEffect(()=>{

		if(seats === 0){
			setIsActive(false)
		}

	},[seats])

	

	function enroll(){

			setCount(count + 1)
			setSeats(seats - 1)

}
	return (


	
		
	
			
			<Card className="Col">
				<Card.Body>
					<Card.Title>
						<h2>{productProp.productName}</h2>
					</Card.Title>
					<Card.Text>
						{productProp.description}
					</Card.Text>
					<Card.Text>
						{productProp.price} Php
					</Card.Text>
					{/*<Button variant="primary" onClick="">Details</Button>*/}
					{/*<Card.Text>
						Enrollees: {

							count === 0 

							? <span className="text-danger">No Enrollees Yet. </span>

							: <span className="text-success">{count}</span>

						}
					</Card.Text>*/}
					<Card.Text>
						Available Pair: {

							seats === 0

							? <span className="text-danger">No more available Pair</span>
							: <span className="text-success">{seats}</span>

						}
					</Card.Text>

					{
						isActive === false

						? <Button variant="primary" disabled>Add Cart</Button>
						: <Button variant="primary" onClick={enroll}>Add Cart</Button>
					}
					
				</Card.Body>
			</Card>

		)
}