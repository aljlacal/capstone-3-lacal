import React, {useState,useEffect,useContext} from 'react'
import {Form,Button,Container,Row,Col} from 'react-bootstrap'


import Swal from 'sweetalert2'

import UserContext from '../userContext'

import {Redirect} from 'react-router-dom'




export default function Login(){


	const {user,setUser} = useContext(UserContext)

	const [email,setEmail] = useState("")
	const [password,setPassword] = useState("")

	
	const [isActive,setIsActive] = useState(false)


	const [willRedirect,setWillRedirect] = useState(false)

	useEffect(()=>{

		if(email !== "" && password !== ""){

			setIsActive(true)

		} else {

			setIsActive(false)

		}


	},[email,password])

	function loginUser(e){

		e.preventDefault()


		fetch('https://floating-basin-77593.herokuapp.com/api/login',{

			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})
		.then(res => res.json())
		.then(data => {

			if(data.message){

				Swal.fire({

					icon: "error",
					title: "Login failed.",
					text: data.message

				})

			} else {
				console.log(data)

	
				localStorage.setItem('token',data.accessToken)

				fetch('https://floating-basin-77593.herokuapp.com/api/profile',{

					headers: {

						Authorization: `Bearer ${data.accessToken}`

					}


				})
				.then(res => res.json())
				.then(data => {

					console.log(data)
				
					localStorage.setItem('email',data.email)
					localStorage.setItem('isAdmin',data.isAdmin)

					setUser({
						email: data.email,
						isAdmin: data.isAdmin 
					})

					
					setWillRedirect(true)
					
					Swal.fire({

						icon: "success",
						title: "Successful Log In!",
						text: `Thank you for logging in, ${data.firstName}`

					})

				})

			}

		
		})


		setEmail("")
		setPassword("")
	
	}



	return (

			user.email || willRedirect
			?
			<Redirect to='/'/>
			:


			<Form onSubmit={e => loginUser(e)}>

			<Container fluid >
				<Row>
					<Col lg={6} md={6} sm={12} className="col">

					<h1 className="log">Login</h1>
				<Form.Group controlId="userEmail">
					<Form.Label>
						Email
					</Form.Label>
					<Form.Control type="email" placeholder="Enter Email" value={email} onChange={(e)=>setEmail(e.target.value)} required />
				</Form.Group>
				<Form.Group controlId="userPassword">
					<Form.Label>
						Password
					</Form.Label>
					<Form.Control type="password" placeholder="Enter Password" value={password} onChange={(e)=>setPassword(e.target.value)}required />
				</Form.Group>
				{

					isActive
					?
					<Button variant="primary" type="submit">Submit</Button>
					:
					<Button variant="primary" disabled>Submit</Button>

				}

					</Col>
				</Row>
			</Container>
			</Form>
		)
}
