import React,{useState,useEffect,useContext} from 'react'

import {Table,Button,Col,Row,Container} from 'react-bootstrap'
import { Redirect } from "react-router-dom";

/*components*/
import Banner from '../components/Banner'
import Product from '../components/Product'

import products from '../data/productsData'

import UserContext from '../userContext'
import '../App.css'


export default function Products(){

	
	const {user} = useContext(UserContext)

	/*input states*/
	const [name,setName] = useState("")
	const [description,setDescription] = useState("")

	
	const [allProducts,setAllProducts] = useState([])
	const [activeProducts,setActiveProducts] = useState([])

	const [update,setUpdate] = useState(0)

	useEffect(()=>{

		//If your request is a get method, there is no need to indicate in an options, our method.
		//If your request is a get method, and there is no need to pass token, we do not have to include authorization in our headers.
		//If your request is a get method, there should be no body of the request, therefore no need to pass body in the options.
		//But if the get method request needs a token, pass an authorization header in the options.


		fetch('https://floating-basin-77593.herokuapp.com/api/products')
		.then(res => res.json())
		.then(data => {

			
			
			setAllProducts(data.data)

			let productsTemp = data.data

			
			let tempArray = productsTemp.filter(product => {

				
				return product.isActive === true

			})

		
			setActiveProducts(tempArray)

		})

	},[update])



	

	let productComponents = activeProducts.map(product => {

		return (
			<Col lg={4} md={4} xs={12} >
      		  	<Product key={product._id} productProp={product}  />  	
			</Col>
				

			)

	

	})

	function archive(productId){

		fetch(`https://floating-basin-77593.herokuapp.com/api/archive/product/${productId}`,{

			method: 'PUT',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			}

		})
		.then(res => res.json())
		.then(data => {

			// console.log(data)
			
			setUpdate({})



		})

	}





	function activate(productId){

		fetch(`https://floating-basin-77593.herokuapp.com/api/active/product/${productId}`,{

			method: 'PUT',
			headers: {

				'Authorization': `Bearer ${localStorage.getItem('token')}`

			}

		})
		.then(res => res.json())
		.then(data => {

			
			setUpdate({})


		})


	}



	function updated (productId){

		

		fetch(`https://floating-basin-77593.herokuapp.com/api/update/product/${productId}`,{

			method: 'PATCH',

			headers: {

				'Content-Type': 'application/json',

				'Authorization': `Bearer ${localStorage.getItem('token')}`

			},

			body: JSON.stringify({

				productName: name,
				description: description,


			})

		})
		.then(res => res.json())
		.then(data => {

			
			setUpdate({})
		})

	}



	
	let productRows = allProducts.map(product => {

		return (

			

				<tr key={product._id}>
					<td>{product._id}</td>
					<td>{product.productName}</td>
					<td className={product.isActive ? "text-success": "text-danger"}>{product.isActive ? "Active":"Inactive"}</td>

					<td>
							<Button variant="info" className="mx-2" onClick={()=>updated(product._id)}>Upadate</Button>
						{
							product.isActive
							?
							<Button variant="danger" className="mx-2" onClick={()=>archive(product._id)}>Archive</Button>
							:
							<Button variant="success" className="mx-2" onClick={()=>activate(product._id)}>Activate</Button>

						}


					</td>
				</tr>


			)

	})



		
			


	







	

	let bannerContent = {

		title: "Welcome to the Naturing Outlet Store",
		description: "Athletic Shoes Collection, Run like you took something",
		label: "Login",
		destination: "/login"

	}

	
	return (


		user.isAdmin === true
		?
		<>
			<h1 className="text-center">Admin Dashboard</h1>
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>ID</th>
						<th>Name</th>
						<th>Status</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{productRows}
				</tbody>
			</Table>
		</>
		:
		<>
		<Container fluid  >
			<Row >
				<Banner bannerProps={bannerContent}/>
				{productComponents}
			</Row>	
		</Container>
	
		</>

		)




}