import React from 'react'

import Banner from '../components/Banner'
import Highlights from '../components/Highlights'




export default function Home(){


	let bannerContent = {

	  title: "If The Shoes Fits Wear It!",
	  description: "Hope you have an on sale abd in your size kind of day!",
	  label: "Shop Now!",
	  destination: "/products"

	}

	return (
		<>
			<Banner bannerProps={bannerContent}/>
			<Highlights />
		</>

		)

}