let products = [
	{
		id: "pro001",
		name: "Stan_Smith Shoes",
		description: "This is good in running",
		price: 4800,
		onOffer: true
	},
	{
		id: "pro002",
		name: "Continental 80 Shoes",
		description: "This is a lowcut shoes.",
		price: 5300,
		onOffer: true
	},
	{
		id: "pro003",
		name: "Forum Low Shoes",
		description: "Combination of 2 colors (White and Blue).",
		price: 4800,
		onOffer: true
	},
	{
		id:"pro004",
		name: "Ultraboost 21 Shoes",
		description: "Good for Running.",
		price: 9500,
		onOffer: true
	}
]


export default products