import React,{useState} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

import {Container} from 'react-bootstrap'


import {BrowserRouter as Router} from 'react-router-dom'
import {Route,Switch} from 'react-router-dom'

/*Components*/
import NavBar from './components/NavBar'
import Banner from './components/Banner'
import Highlights from './components/Highlights'
import Product from './components/Product'

/*Pages*/
import Home from './pages/home'
import Register from './pages/register'
import Login from './pages/login'
import Products from './pages/products'
import AddProduct from './pages/addProduct'
import NotFound from './pages/notFound'


/*Provider*/
import {UserProvider} from './userContext'

/*product data*/
import products from './data/productsData'

/*images*/
// import image from '../images/adidaslogo.jpg'



function App(){

	const [user,setUser] = useState({

    email: localStorage.getItem('email'),
    isAdmin: JSON.parse(localStorage.getItem('isAdmin'))


  })

function unsetUser(){

  localStorage.clear()

}

	  return(

	  	<>
		  	<UserProvider value={{user,setUser,unsetUser}}>
		  		<Router>
		  		<NavBar />
		  		<div className="background">
		  			<Container>
		  				<Switch>
		  					<Route exact path="/" component={Home}/>
		  					<Route exact path="/register" component={Register} />
		  					<Route exact path="/login" component={Login} />
		  					<Route exact path="/products" component={Products} />
		  					<Route exact path="/addProduct" component={AddProduct} />
		  					<Route component={NotFound} />
		  				</Switch>
		  			</Container>
		  			</div>
		  		</Router>
		  	 </UserProvider>
	  	</>
		     
		)
}



export default App;
